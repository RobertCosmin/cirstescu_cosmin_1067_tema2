import React from 'react';
import axios from 'axios';

export class AddProduct extends React.Component {
    constructor(props){
        super(props);
        this.state = {};
        this.state.productName = ""
        this.state.price = 0;
    }
    
    
    handleChangeProductName = (event) => {
        this.setState({
            productName: event.target.value
        })
    }
    
    handleChangeProductPrice = (event) => {
        this.setState({
            price: event.target.value
        })
    }
    
    handleAddClick = () => {
        let product = {
            productName: this.state.productName,
            price: this.state.price
        }
        axios.post('https://myworkspace-robertcosmin.c9users.io/add', product).then((res) => {
            if(res.status === 200){
                this.props.productAdded(product)
            }
        }).catch((err) =>{
            console.log(err)
        })
    }
    render(){
        return(
            <div>
                <h1>Add product</h1>
                <input type="text" placeholder="Product Name" 
                    onChange={this.handleChangeProductName}
                    value={this.state.productName} />
                <input type="number" placeholder="Product price" 
                    onChange={this.handleChangeProductPrice}
                    value={this.state.price} />
                <button onClick={this.handleAddClick}>Add product</button>
            </div>
            );
    }
}