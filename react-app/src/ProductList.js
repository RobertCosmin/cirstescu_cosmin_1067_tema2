import React from 'react';

export class ProductList extends React.Component {
    
    
    render(){
        let products = this.props.source.map((product, index) =>{
            return <div key={index}>{product.productName} {product.price} </div>
            
        })
        return(
            <div>
                <h1>{this.props.title}</h1>
                <div>
                {products}
                </div>
            </div>
            );
    }
}
