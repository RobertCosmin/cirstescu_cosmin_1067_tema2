import React, { Component } from 'react';
//import logo from './logo.svg';
import './App.css';

import {ProductList} from './ProductList';
import {AddProduct} from './AddProduct';

class App extends Component {

  constructor(props){
    super(props)
    this.state = {};
    this.state.products = [];
  }

  onItemAdded = (product) => {
    let products = this.state.products;
    products.push(product);
    this.setState({
      products: products
    })
    console.log(this.state.products)
  }
  componentWillMount(){
    fetch('https://myworkspace-robertcosmin.c9users.io/get-all')
    .then((res) => 
        res.json())
    .then((products) =>{
      this.setState({
        products: products
      })
    })
  }

  render() {
    return (
      <React.Fragment>
      <h1>Products App</h1>
      <AddProduct productAdded={this.onItemAdded}></AddProduct>
      <div className="lists-container">
      <ProductList title="Products" source={this.state.products}></ProductList>
      </div>
      </React.Fragment>
    );
  }
}

export default App;
